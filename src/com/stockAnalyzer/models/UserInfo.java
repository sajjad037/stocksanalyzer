package com.stockAnalyzer.models;

/**
 * Model Class for User information.
 * 
 * @author SajjadAshrafCan
 *
 */
public class UserInfo {

	private String userName;
	private String email;
	private String fullName;
	private String password;

	/**
	 * 
	 * @param userName
	 * @param email
	 * @param fullName
	 * @param password
	 */
	public UserInfo(String userName, String email, String fullName, String password) {
		super();
		this.userName = userName;
		this.email = email;
		this.fullName = fullName;
		this.password = password;
	}

	/**
	 * 
	 * @param userName
	 * @param email
	 * @param fullName
	 * @param password
	 */
	public UserInfo(String userName, String password) {
		super();
		this.userName = userName;
		this.password = password;
	}

	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * @param userName
	 *            the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email
	 *            the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the fullName
	 */
	public String getFullName() {
		return fullName;
	}

	/**
	 * @param fullName
	 *            the fullName to set
	 */
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password
	 *            the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "UserInfo [userName=" + userName + ", email=" + email + ", fullName=" + fullName + ", password="
				+ password + "]";
	}

	public String getHtml(String emailType) {
		StringBuilder sb = new StringBuilder();
		sb.append("<p><b>Your " + emailType + " Information :</b></p>").append("<table>").append("<tr>")
				.append("<td>Full Name </td><td>" + this.getFullName() + "</td>").append("</tr>").append("<tr>")
				.append("<td>User Name </td><td>" + this.getUserName() + "</td>").append("</tr>").append("<tr>")
				.append("<td>Email     </td><td>" + this.getEmail() + "</td>").append("</tr>").append("<tr>")
				.append("<td>Password  </td><td>" + this.getPassword() + "</td>").append("</tr>").append("</table>");
		return sb.toString();
	}

}
