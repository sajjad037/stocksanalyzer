package com.stockAnalyzer.models;

import java.util.Date;

/**
 * Model Class Stock details data
 * @author SajjadAshrafCan
 *
 */
public class StockDetail {

	private String stockName;
	private Date date;
	private double open;
	private double high;
	private double low;
	private double close;
	private double volume;
	private double adjClose;
	private double avgClose1;
	private double avgClose2;
	private String decision;

	/**
	 * @param stockName
	 * @param date
	 * @param open
	 * @param high
	 * @param low
	 * @param close
	 * @param volume
	 * @param adjClose
	 */
	public StockDetail(String stockName, Date date, double open, double high, double low, double close, double volume,
			double adjClose) {
		super();
		this.stockName = stockName;
		this.date = date;
		this.open = open;
		this.high = high;
		this.low = low;
		this.close = close;
		this.volume = volume;
		this.adjClose = adjClose;
	}

	/**
	 * @return the stockName
	 */
	public String getStockName() {
		return stockName;
	}

	/**
	 * @param stockName
	 *            the stockName to set
	 */
	public void setStockName(String stockName) {
		this.stockName = stockName;
	}

	/**
	 * @return the date
	 */
	public Date getDate() {
		return date;
	}

	/**
	 * @param date
	 *            the date to set
	 */
	public void setDate(Date date) {
		this.date = date;
	}

	/**
	 * @return the open
	 */
	public double getOpen() {
		return open;
	}

	/**
	 * @param open
	 *            the open to set
	 */
	public void setOpen(double open) {
		this.open = open;
	}

	/**
	 * @return the high
	 */
	public double getHigh() {
		return high;
	}

	/**
	 * @param high
	 *            the high to set
	 */
	public void setHigh(double high) {
		this.high = high;
	}

	/**
	 * @return the low
	 */
	public double getLow() {
		return low;
	}

	/**
	 * @param low
	 *            the low to set
	 */
	public void setLow(double low) {
		this.low = low;
	}

	/**
	 * @return the close
	 */
	public double getClose() {
		return close;
	}

	/**
	 * @param close
	 *            the close to set
	 */
	public void setClose(double close) {
		this.close = close;
	}

	/**
	 * @return the volume
	 */
	public double getVolume() {
		return volume;
	}

	/**
	 * @param volume
	 *            the volume to set
	 */
	public void setVolume(double volume) {
		this.volume = volume;
	}

	/**
	 * @return the adjClose
	 */
	public double getAdjClose() {
		return adjClose;
	}

	/**
	 * @param adjClose
	 *            the adjClose to set
	 */
	public void setAdjClose(double adjClose) {
		this.adjClose = adjClose;
	}

	/**
	 * @return the avgClose1
	 */
	public double getAvgClose1() {
		return avgClose1;
	}

	/**
	 * @param avgClose1
	 *            the avgClose1 to set
	 */
	public void setAvgClose1(double avgClose1) {
		this.avgClose1 = avgClose1;
	}

	/**
	 * @return the avgClose2
	 */
	public double getAvgClose2() {
		return avgClose2;
	}

	/**
	 * @param avgClose2
	 *            the avgClose2 to set
	 */
	public void setAvgClose2(double avgClose2) {
		this.avgClose2 = avgClose2;
	}

	/**
	 * @return the decision
	 */
	public String getDecision() {
		return decision;
	}

	/**
	 * @param decision
	 *            the decision to set
	 */
	public void setDecision(String decision) {
		this.decision = decision;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "stockDetail [stockName=" + stockName + ", date=" + date + ", open=" + open + ", high=" + high + ", low="
				+ low + ", close=" + close + ", volume=" + volume + ", adjClose=" + adjClose + "]";
	}

}
