package com.stockAnalyzer.models;

/**
 * Model Class for Stock price.
 * 
 * @author SajjadAshrafCan
 *
 */
public class StockPrice {

	private String symbol;
	private String name;
	private double bid;
	private double ask;
	private double open;
	private double previousClose;
	private double last;

	/**
	 * @param symbol
	 * @param name
	 * @param bid
	 * @param ask
	 * @param open
	 * @param previousClose
	 * @param last
	 */
	public StockPrice(String symbol, String name, double bid, double ask, double open, double previousClose,
			double last) {
		super();
		this.symbol = symbol;
		this.name = name;
		this.bid = bid;
		this.ask = ask;
		this.open = open;
		this.previousClose = previousClose;
		this.last = last;
	}

	/**
	 * @return the symbol
	 */
	public String getSymbol() {
		return symbol;
	}

	/**
	 * @param symbol
	 *            the symbol to set
	 */
	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the bid
	 */
	public double getBid() {
		return bid;
	}

	/**
	 * @param bid
	 *            the bid to set
	 */
	public void setBid(double bid) {
		this.bid = bid;
	}

	/**
	 * @return the ask
	 */
	public double getAsk() {
		return ask;
	}

	/**
	 * @param ask
	 *            the ask to set
	 */
	public void setAsk(double ask) {
		this.ask = ask;
	}

	/**
	 * @return the open
	 */
	public double getOpen() {
		return open;
	}

	/**
	 * @param open
	 *            the open to set
	 */
	public void setOpen(double open) {
		this.open = open;
	}

	/**
	 * @return the previousClose
	 */
	public double getPreviousClose() {
		return previousClose;
	}

	/**
	 * @param previousClose
	 *            the previousClose to set
	 */
	public void setPreviousClose(double previousClose) {
		this.previousClose = previousClose;
	}

	/**
	 * @return the last
	 */
	public double getLast() {
		return last;
	}

	/**
	 * @param last
	 *            the last to set
	 */
	public void setLast(double last) {
		this.last = last;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "StockPrice [symbol=" + symbol + ", name=" + name + ", bid=" + bid + ", ask=" + ask + ", open=" + open
				+ ", previousClose=" + previousClose + ", last=" + last + "]";
	}

}
