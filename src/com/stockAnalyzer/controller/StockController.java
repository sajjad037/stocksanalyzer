package com.stockAnalyzer.controller;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;

import org.jfree.data.time.Day;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.xy.XYDataset;

import com.stockAnaluzer.persistance.APIFactory;
import com.stockAnaluzer.persistance.IBaselFinanceAPI;
import com.stockAnaluzer.persistance.SingletonPersistance;
import com.stockAnalyzer.models.StockDetail;
import com.stockAnalyzer.models.StockPrice;
import com.stockAnalyzer.models.UserInfo;
import com.stockAnalyzer.utilities.ApplicationStatics;
import com.stockAnalyzer.utilities.EmailSend;
import com.stockAnalyzer.utilities.Enums.FinanceAPI;
import com.stockAnalyzer.utilities.InputValidation;

/**
 * Controller of Application
 * 
 * @author SajjadAshrafCan
 *
 */
public class StockController {

	SingletonPersistance persistance;
	String dateFormat = ApplicationStatics.DATE_FORMAT;
	FinanceAPI financeAPI = FinanceAPI.Yahoo;

	/**
	 * Constructor
	 * 
	 * @throws Exception
	 */
	public StockController() throws Exception {
		// TODO Auto-generated constructor stub
		persistance = SingletonPersistance.getInstace(ApplicationStatics.USER_INFO_DATA_PATH,
				ApplicationStatics.CSV_FILE_PATH, ApplicationStatics.WATCH_LIST_FILE_PATH,
				ApplicationStatics.DATE_FORMAT);
	}

	/**
	 * Get Directory Path, directory which would contains all file created or
	 * used by application.
	 * 
	 * @return Path of Directory
	 */
	public static String getDirectoryPath() {
		return ApplicationStatics.directoryPath;
	}

	/**
	 * Validate user.
	 * 
	 * @param userName
	 * @param password
	 * @return
	 */
	public String validateUser(String userName, String password) {
		UserInfo userInfo = new UserInfo(userName, password);
		String status = "";

		if (!InputValidation.hasAlphaNoSpace(userName)) {
			status = "User Name can only Alpha numeric with out spaces.";
		} else {
			if (persistance.validUser(userInfo)) {
				status = "OK";
			} else {
				status = "Login Failed, wrong userName and password.";
			}
		}
		return status;
	}

	/**
	 * Get User Password via Email Address.
	 * 
	 * @param email
	 * @return
	 */
	public String forgotPasswordEmail(String email) {
		String status = "Email not exist!";

		if (!InputValidation.hasValidEmail(email)) {
			status = "Please enter Valid Email Address.";
		} else {
			UserInfo user = persistance.getUserInfo(email);
			if (user != null) {

				status = "Failed to sent Email";
				EmailSend sendGrid = new EmailSend();
				String msgBody = "Your Credential Information : \r\n" + "User Name : " + user.getUserName() + "\r\n"
						+ "Password : " + user.getPassword() + "\r\n";
				boolean value = sendGrid.send(ApplicationStatics.FROM_EMAIL_ADDRESS, user.getEmail(),
						"Forgot password Request", user.getHtml("Credential"), ApplicationStatics.SEND_GRID_API_KEY);
				if (value) {
					status = "OK: An email has sent to your account (" + email + ") with credtial inforamtion ";
				} else {
					status = "OK:" + msgBody;
				}
			}
		}

		return status;
	}

	/**
	 * Add new User.
	 * 
	 * @param userName
	 * @param email
	 * @param fullName
	 * @param password
	 * @return
	 */
	public String addUser(String userName, String email, String fullName, String password) {
		String status = "";

		if (!InputValidation.hasAlphaNoSpace(userName)) {
			status = "Please enter valid user Name.";
		} else if (!InputValidation.hasValidEmail(email)) {
			status = "Please enter Valid Email Address.";
		} else if (!InputValidation.hasAlphaWithSpace(fullName)) {
			status = "Please enter correct Full Name.";
		} else {
			UserInfo userInfo = new UserInfo(userName, email, fullName, password);
			if (persistance.hasALready(userInfo, "Email")) {
				status = "Email " + userInfo.getEmail() + " is already exist!, Please Enter Differernt Email Address.";
			} else if (persistance.hasALready(userInfo, "UserName")) {
				status = "User name " + userInfo.getUserName()
						+ " is already exist!, Please Enter Differernt User name.";
			} else {
				status = persistance.addUser(userInfo);
				if (status.contains("OK")) {
					EmailSend sendGrid = new EmailSend();
					sendGrid.send(ApplicationStatics.FROM_EMAIL_ADDRESS, userInfo.getEmail(),
							"Account Creation Request",
							"Your accunt successfully! Creaeted.<br/>" + userInfo.getHtml("Account"),
							ApplicationStatics.SEND_GRID_API_KEY);
					status = "OK";
				}
			}
		}
		return status;
	}

	/**
	 * validate stock rating inputs.
	 * 
	 * @param symbol
	 * @param strFromDate
	 * @param strToDate
	 * @return
	 */
	private String validateInputsForStockRating(String symbol, String strFromDate, String strToDate) {
		String status = "";
		String dateFormat = ApplicationStatics.DATE_FORMAT;

		if (!InputValidation.hasAlphaNoSpace(symbol)) {
			status = "Please enter Valid symbol.";
		} else if (!InputValidation.hasValidDateTime(strFromDate, dateFormat)) {
			status = "Please enter Valid From date (MM/dd/yyyy).";
		} else if (!InputValidation.hasValidDateTime(strToDate, dateFormat)) {
			status = "Please enter Valid To date (MM/dd/yyyy).";
		} else if (!InputValidation.hasFromBeforeToDate(strFromDate, strToDate, dateFormat)) {
			status = "From date must be before To Date.";
		} else {
			status = "OK";
		}
		return status;
	}

	/**
	 * Get stock history data, with moving averages.
	 * 
	 * @param symbol
	 * @param strFromDate
	 * @param strToDate
	 * @param range1
	 * @param range2
	 * @param errorMsg
	 * @return
	 * @throws Exception
	 */
	public ArrayList<StockDetail> getStockDetails(String symbol, String strFromDate, String strToDate, int range1,
			int range2, StringBuilder errorMsg) throws Exception {

		String status = validateInputsForStockRating(symbol, strFromDate, strToDate);
		if (status.equals("OK")) {
			errorMsg.append(status);
			Date fromDate = InputValidation.getDate(strFromDate, dateFormat);
			Date toDate = InputValidation.getDate(strToDate, dateFormat);
			IBaselFinanceAPI yahooAPI = new APIFactory(dateFormat).getAPI(financeAPI);
			int maxRange = range1 > range2 ? range1 : range2;

			// Get data from Yahoo API
			ArrayList<StockDetail> listOfStocks = yahooAPI.getStockDetail(symbol, fromDate, toDate, maxRange);
			persistance.setListOfStocks(listOfStocks);

			// Filter data
			ArrayList<StockDetail> stockDetail = persistance.getStockData(fromDate, toDate);

			// Get Ist Range
			stockDetail = getStockDetailRange1(stockDetail, range1);

			// Get 2nd Range
			stockDetail = getStockDetailRange2(stockDetail, range2);

			// Moving average decision
			// double oldShortAvg = 0.0, oldLongAvg = 0.0;
			double curShortAvg, curLongAvg = 0.0;
			boolean isLongGreater = true;
			boolean oldTrend = true;
			for (int i = 0; i < stockDetail.size(); i++) {

				curShortAvg = stockDetail.get(i).getAvgClose1();
				curLongAvg = stockDetail.get(i).getAvgClose2();
				// Set intial Trands
				if (i == 0) {
					if (curShortAvg > curLongAvg) {
						isLongGreater = false;
						oldTrend = isLongGreater;
					}
					stockDetail.get(i).setDecision(" - ");
					// oldShortAvg = curShortAvg;
					// oldLongAvg = curLongAvg;

				} else {

					if (curShortAvg > curLongAvg) {
						isLongGreater = false;
					} else {
						isLongGreater = true;
					}

					if (isLongGreater == oldTrend) {
						oldTrend = isLongGreater;
						stockDetail.get(i).setDecision(" - ");
					} else {
						oldTrend = isLongGreater;
						//
						System.out.println("Trend Change at index:" + i + ", values Short:" + curShortAvg + ", Long:"
								+ curLongAvg);
						String decision = isLongGreater ? "Buy" : "Sell";
						stockDetail.get(i).setDecision(decision);

					}
					// oldShortAvg = curShortAvg;
					// oldShortAvg = curLongAvg;
				}
			}

			return stockDetail;
		} else {
			errorMsg.append(status);
			return null;
		}
	}

	/**
	 * Get stock history data, for range 1 (Shorter).
	 * 
	 * @param stockDetail
	 * @param range
	 * @return
	 * @throws Exception
	 */
	private ArrayList<StockDetail> getStockDetailRange1(ArrayList<StockDetail> stockDetail, int range)
			throws Exception {
		return persistance.getStockRangeData(stockDetail, range, "Range1");

	}

	/**
	 * Get stock history data, for range 2 (Longer).
	 * 
	 * @param stockDetail
	 * @param range
	 * @return
	 * @throws Exception
	 */
	private ArrayList<StockDetail> getStockDetailRange2(ArrayList<StockDetail> stockDetail, int range)
			throws Exception {
		return persistance.getStockRangeData(stockDetail, range, "Range2");

	}

	/**
	 * Create Dataset used to display data on graph
	 * 
	 * @param stockDetail
	 * @param range1
	 * @param range2
	 * @return
	 */
	public XYDataset createDataset(ArrayList<StockDetail> stockDetail, int range1, int range2) {

		TimeSeries dateSeries = new TimeSeries("Data");
		TimeSeries range1Series = new TimeSeries("Range " + range1);
		TimeSeries range2Series = new TimeSeries("Range " + range2);
		for (StockDetail stock : stockDetail) {
			dateSeries.add(new Day(stock.getDate()), stock.getClose());
			range1Series.add(new Day(stock.getDate()), stock.getAvgClose1());
			range2Series.add(new Day(stock.getDate()), stock.getAvgClose2());
		}

		TimeSeriesCollection dataset = new TimeSeriesCollection();
		dataset.addSeries(dateSeries);
		dataset.addSeries(range1Series);
		dataset.addSeries(range2Series);

		return dataset;

	}

	// /**
	// * Get Tale module, used to display data in Grid.
	// *
	// * @param stockDetail
	// * @param range1
	// * @param range2
	// * @return
	// */
	// private void getTableModel(DefaultTableModel model,
	// ArrayList<StockDetail> stockDetail, int range1, int range2) {
	// String[] columns = { "S. #", "Date", "Close", "Short AVG(" + range1 + "
	// Days)",
	// "Long AVG (" + range2 + " Days)", "Decision" };
	// model.setColumnIdentifiers(columns);
	//
	// DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
	// for (int i = 0; i < stockDetail.size(); i++) {
	//
	// model.addRow(new Object[] { (i + 1),
	// dateFormat.format(stockDetail.get(i).getDate()),
	// stockDetail.get(i).getClose(), stockDetail.get(i).getAvgClose1(),
	// stockDetail.get(i).getAvgClose2(),
	// stockDetail.get(i).getDecision() });
	// }
	// }

	/**
	 * Get stock current price.
	 * 
	 * @param symbolName
	 * @return
	 * @throws IOException
	 */
	public StockPrice getCurrentStockPrice(String symbolName) throws IOException {
		IBaselFinanceAPI yahooAPI = new APIFactory(dateFormat).getAPI(financeAPI);
		return yahooAPI.getCurrentStockPrice(symbolName);
	}

	/**
	 * Save watch list symbols.
	 * 
	 * @param symbols
	 * @return
	 */
	public String saveWatchListSymbols(String symbols) {
		String details = getWatchListDetails();
		if (details.contains("ERROR")) {
			details = "";
		}
		return saveWatchList(symbols, details);
	}

	/**
	 * Save watch list details.
	 * 
	 * @param details
	 * @return
	 */
	private String saveWatchListDetails(String details) {
		String symbols = getWatchListSymbols();
		if (symbols.contains("ERROR")) {
			symbols = "";
		}
		return saveWatchList(symbols, details);
	}

	/**
	 * Save watch list.
	 * 
	 * @param symbols
	 * @param details
	 * @return
	 */
	private String saveWatchList(String symbols, String details) {
		String data = String.format("%s~%s", symbols, details);
		return persistance.saveWatchList(data);
	}

	/**
	 * Get watch list symbols.
	 * 
	 * @return
	 */
	public String getWatchListSymbols() {
		String watchListData = getWatchList();
		if (watchListData.contains("ERROR")) {
			return watchListData;
		}
		String[] data = watchListData.split("~");
		return data.length > 0 ? data[0] : "";
	}

	/**
	 * Get watch list details.
	 * 
	 * @return
	 */
	private String getWatchListDetails() {
		String watchListData = getWatchList();
		if (watchListData.contains("ERROR")) {
			return watchListData;
		}
		String[] data = watchListData.split("~");
		return data.length > 1 ? data[1] : "";
	}

	/**
	 * Get watch list.
	 * 
	 * @return
	 */
	private String getWatchList() {
		return persistance.getWatchList();
	}

	/**
	 * Get watch list data.
	 * 
	 * @return
	 * @throws Exception
	 */
	public String getWatchListData() throws Exception {
		StringBuilder details = new StringBuilder();
		String symbols = getWatchListSymbols();
		if (symbols.equals("") || symbols.contains("ERROR")) {
			details.append(
					"Watch list is empty. select from Top Menu, File -> WatchList to add symbol for watch Lits!.");
		} else {
			String[] symbolArry = symbols.split(",");
			StringBuilder msg = new StringBuilder();
			DateFormat df = new SimpleDateFormat(dateFormat);
			Calendar cal = Calendar.getInstance();
			Date toDate = cal.getTime();
			String strToDate = df.format(toDate);

			cal.add(Calendar.YEAR, -1); // to get previous year add -1
			Date fromDate = cal.getTime();
			String strFromDate = df.format(fromDate);

			for (int i = 0; i < symbolArry.length; i++) {
				String symbol = symbolArry[i].split(" - ")[0].trim();

				ArrayList<StockDetail> stockDetail = getStockDetails(symbol, strFromDate, strToDate, 10, 100, msg);
				if (msg.toString().equals("OK")) {
					msg = new StringBuilder();
					Collections.reverse(stockDetail);

					// Collections.sort(stockDetail, new
					// Comparator<StockDetail>() {
					// @Override
					// public int compare(StockDetail o1, StockDetail o2) {
					// return o2.getDate().compareTo(o1.getDate());
					// }
					// });
					boolean gotDecision = false;
					for (StockDetail stock : stockDetail) {
						if (stock.getDecision() != null && !stock.getDecision().equals(" - ")) {
							details.append(String.format("%s - %s on %s, ", symbol, stock.getDecision(),
									df.format(stock.getDate())));
							gotDecision = true;
							break;
						}
					}
					if (!gotDecision) {
						details.append(String.format("%s - No Decsion found, ", symbol));
					}
				} else {
					details.append(msg.toString());
					msg = new StringBuilder();
				}
			}
		}

		saveWatchListDetails(details.toString());
		return details.toString();
	}

}
