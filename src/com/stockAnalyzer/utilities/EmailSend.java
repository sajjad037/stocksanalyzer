package com.stockAnalyzer.utilities;

import java.io.IOException;

import com.sendgrid.Content;
import com.sendgrid.Email;
import com.sendgrid.Mail;
import com.sendgrid.Method;
import com.sendgrid.Request;
import com.sendgrid.Response;
import com.sendgrid.SendGrid;

/**
 * Helpful to send emails 
 * @author SajjadAshrafCan
 *
 */
public class EmailSend {

	/**
	 * Constructor. 
	 */
	public EmailSend() {
	}
	
	/**
	 * Send email
	 * @param fromEmail
	 * @param toEmail
	 * @param subj
	 * @param body
	 * @param apiKey
	 * @return
	 */
	public boolean send(String fromEmail, String toEmail, String subj, String body, String apiKey)
	{
		boolean status =false;

		Email from = new Email(fromEmail);
	    String subject = subj;
	    Email to = new Email(toEmail);
	    Content content = new Content("text/html", body);
	    Mail mail = new Mail(from, subject, to, content);
	    
	    SendGrid sg = new SendGrid(apiKey);
	    Request request = new Request();
	    try {
	      request.method = Method.POST;
	      request.endpoint = "mail/send";
	      request.body = mail.build();
	      Response response = sg.api(request);
	      if(response.statusCode == 200 || response.statusCode == 202 )
	      {
	    	  status = true;
	      }
	    } catch (IOException ex) {
	    	status = false;
	    	ex.printStackTrace();
	    }	    
		return status;		
	}

}
