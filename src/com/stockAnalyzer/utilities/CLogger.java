package com.stockAnalyzer.utilities;

import java.util.logging.ConsoleHandler;
import java.util.logging.FileHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

/**
 * Logger
 * 
 * @author SajjadAshrafCan
 *
 */
public class CLogger {
	private Logger LOGGER = null;
	String logFilePath = "";
	static private FileHandler fileHandler;
	static private SimpleFormatter simpleFormatter;
	
	/**
	 * Constructor 
	 * @param LOGGER
	 */
	public CLogger(Logger LOGGER) {
		try {
			this.LOGGER = LOGGER;
			this.logFilePath = ApplicationStatics.LOG_PATH;

			LOGGER.setLevel(Level.INFO);
			LOGGER.setUseParentHandlers(false);
			fileHandler = new FileHandler(this.logFilePath, true);

			 simpleFormatter = new SimpleFormatter();
			fileHandler.setFormatter(simpleFormatter);
			
			LOGGER.addHandler(fileHandler);			

            Handler[] handlers = LOGGER.getHandlers();
            if (handlers[0] instanceof ConsoleHandler) {
            	LOGGER.removeHandler(handlers[0]);
            }
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	/**
	 * Log Normal msg
	 * 
	 * @param msg
	 */
	public void log(String msg) {
		LOGGER.info(msg);
	}

	/**
	 * Log Exception
	 * 
	 * @param msg
	 * @param ex
	 */
	public void logException(String msg, Exception ex) {
		
		LOGGER.log(Level.SEVERE, msg.equals("") ? "an exception was thrown" : msg, ex);
	}
}
