package com.stockAnalyzer.utilities;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Pattern;

/**
 * This class validate user inputs.
 * 
 * @author SajjadAshrafCan
 *
 */
public class InputValidation {

	private static final Pattern NUMBERS = Pattern.compile("\\d+");
	private static final Pattern LETTERS_WITHOUT_SPACES = Pattern.compile("\\w+$");//
	private static final Pattern LETTERS_WITH_SPACES = Pattern.compile("[a-zA-Z ]*");
	private static final Pattern EMAIL_PATTERN = Pattern
			.compile("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");

	/**
	 * is Numeric
	 * 
	 * @param text
	 * @return
	 */
	public static final boolean hasNumeric(String text) {
		return NUMBERS.matcher(text).matches();
	}

	/**
	 * is Phone Number
	 * 
	 * @param text
	 * @return
	 */
	public static final boolean hasPhoneNumber(String text) {
		return NUMBERS.matcher(text).matches();
	}

	/**
	 * is Alpha numeric string with No Space
	 * 
	 * @param text
	 * @return
	 */
	public static final boolean hasAlphaNoSpace(String text) {
		return LETTERS_WITHOUT_SPACES.matcher(text).matches();
	}

	/**
	 * is Alpha numeric string with Space
	 * 
	 * @param text
	 * @return
	 */
	public static final boolean hasAlphaWithSpace(String text) {
		return LETTERS_WITH_SPACES.matcher(text).matches();
	}

	/**
	 * is String Has Valid DateTime
	 * 
	 * @param strDate
	 * @param format
	 * @return
	 */
	public static boolean hasValidDateTime(String strDate, String format) {
		boolean status = false;
		try {
			DateFormat dateFormat = new SimpleDateFormat(format);
			Date output = dateFormat.parse(strDate);
			String value = dateFormat.format(output);
			status = strDate.equals(value);
		} catch (Exception ignore) {
		}

		return status;
	}

	public static final boolean hasValidEmail(String text) {
		return EMAIL_PATTERN.matcher(text).matches();
	}

	public static final Date getDate(String strDate, String format) {
		DateFormat df = new SimpleDateFormat(format);
		try {
			return df.parse(strDate);
		} catch (ParseException e) {
			e.printStackTrace();
			return null;
		}
	}

	public static final boolean hasFromBeforeToDate(String strFromDate, String strToDate, String dateFormat) {
		Date fromdate = getDate(strFromDate, dateFormat);
		Date todate = getDate(strToDate, dateFormat);
		return fromdate.before(todate);
	}

}
