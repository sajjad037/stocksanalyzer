package com.stockAnalyzer.utilities;

/**
 * This class contains all constant, Final and Static variables such as file
 * path, formats, date time format and api keys.
 * 
 * @author Sajjad Ashraf
 * 
 */
public class ApplicationStatics {

	public static final String directoryPath = "C:\\StockAnalyzer";
	public static final String CSV_FILE_PATH = directoryPath + "\\Sample data.csv";
	public static final String WATCH_LIST_FILE_PATH = directoryPath + "\\WatchList.txt";
	public static final String USER_INFO_DATA_PATH = directoryPath + "\\UserInfoData.txt";
	public static final String LOG_PATH = directoryPath + "\\log.txt";
	public static final String LOG_PATTERN = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX";
	public static final String DATE_FORMAT = "MM/dd/yyyy";
	public static final String TIME_FORMAT = "HH;mm";
	public static final String DATE_FORMAT_DEFAULT = "yyyyMMddHHmmssSSS";
	public static final String SEND_GRID_API_KEY = "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";
	public static final String FROM_EMAIL_ADDRESS = "noreplystockanalyzer@gmail.com";
}
