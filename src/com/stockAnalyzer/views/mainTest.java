package com.stockAnalyzer.views;

import java.util.Date;

import com.stockAnalyzer.utilities.ApplicationStatics;
import com.stockAnalyzer.utilities.InputValidation;

/**
 * Test class, here use test the unit of code before we implement it.
 * @author SajjadAshrafCan
 *
 */
public class mainTest {

	public mainTest() {
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) {

		// TODO Auto-generated method stub
		// ArrayList<UserInfo> usersList = new ArrayList<UserInfo>();
		// usersList.add(new UserInfo("Sajjad037", "sabc@yahoo.com", "awan",
		// "123456"));
		// usersList.add(new UserInfo("Sajjad038", "sabc@yahoo.com", "awan",
		// "123456"));

		// String value = new FileStorage().getJsonFromObject(usersList);
		// ArrayList<UserInfo> objectFromJson = (ArrayList<UserInfo>) new
		// FileStorage().getObjectFromJson(value, usersList.getClass());
		// usersList = objectFromJson;
		// File file = new File(ApplicationStatics.USER_INFO_DATA_PATH);
		// //System.out.println(new FileStorage().saveUserInfo(file,
		// usersList));
		//
		// ArrayList<UserInfo> info = new FileStorage().loadUserInfo(file);
		// System.out.println(info.get(0).getEmail());
		// System.out.println("done");
		//
		/*
		 * Email from = new Email("sajjad037@yahoo.com"); String subject =
		 * "Hello World from the SendGrid Java Library!"; Email to = new
		 * Email("ssajjadashraf@gmail.com"); Content content = new
		 * Content("text/plain", "Hello, Email!"); Mail mail = new Mail(from,
		 * subject, to, content); // var sg =
		 * require('sendgrid')('SG.XXXXXXXXXXXXXXXXXXX'); SendGrid sg = new
		 * SendGrid(
		 * "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"
		 * ); Request request = new Request(); try { request.method =
		 * Method.POST; request.endpoint = "mail/send"; request.body =
		 * mail.build(); Response response = sg.api(request);
		 * System.out.println(response.statusCode);
		 * System.out.println(response.body);
		 * System.out.println(response.headers); } catch (IOException ex) { //
		 * throw ex; ex.printStackTrace(); }
		 */

		/*
		 * Configuration configuration = new Configuration()
		 * .domain("somedomain.com")
		 * .apiKey("key-bd11670b6ed8fc09035af8e067decfb9") .from("Test account",
		 * "postmaster@somedomain.com");
		 * 
		 * Response mb = Mail.using(configuration) .to("marty@mcfly.com")
		 * .subject("This is the subject") .text("Hello world!") .build()
		 * .send();
		 * 
		 * System.out.println(mb.responseCode());
		 * System.out.println(mb.responseMessage());
		 * 
		 */
		
		// Avg Trend
		double shortData[] = {1, 2, 3, 4, 2, 1, 0.5};
		double longData[] = {5, 4, 2.5, 2, 3, 5, 5.5};
		
		//Moving average decision
		//double oldShortAvg =0.0, oldLongAvg = 0.0; 
		double curShortAvg, curLongAvg= 0.0;
		boolean isLongGreater =true;
		boolean oldTrend = true;
		for (int i = 0; i < shortData.length; i++) {
			
			curShortAvg = shortData[i];
			curLongAvg = longData[i];	
			
			//Set intial Trands
			if(i==0){					
				if(curShortAvg > curLongAvg)
				{
					isLongGreater = false;
					oldTrend = isLongGreater;
				}
				//oldShortAvg = curShortAvg;
				//oldLongAvg = curLongAvg;
				
			}
			else
			{
			
				if(curShortAvg > curLongAvg)
				{
					isLongGreater = false;
				}
				else
				{
					isLongGreater = true;
				}
				
				if(isLongGreater == oldTrend)
				{
					oldTrend = isLongGreater;
				}
				else
				{
					oldTrend = isLongGreater;
					//
					System.out.println("Trend Change at index:"+i+", values Short:"+curShortAvg+", Long:"+curLongAvg+", Decsion : "+ (isLongGreater ? "Buy" : "Sell"));
				}
				
				
				//oldShortAvg = curShortAvg;
				//oldShortAvg = curLongAvg;
			}			
		}
		
		

		Date fromdate = InputValidation.getDate("11/05/2010", ApplicationStatics.DATE_FORMAT);
		Date todate = InputValidation.getDate("10/10/2010", ApplicationStatics.DATE_FORMAT);
		System.out.println(fromdate.before(todate));

	}
}
