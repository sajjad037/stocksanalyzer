package com.stockAnalyzer.views;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.border.EmptyBorder;

import com.stockAnalyzer.controller.StockController;

/**
 * Stock watch list screen.
 * @author SajjadAshrafCan
 *
 */
public class StockWatchList extends BaseView {
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private static String VIEW_NAME = StockWatchList.class.getName();
	private JList<String> lstSymbols;

	/**
	 * Constructor.
	 * Create the frame.
	 */
	public StockWatchList() {
		super(VIEW_NAME);
		setTitle("Stock Watch List");
		setBounds(100, 100, 385, 398);

		setResizable(false);
		centreWindow(this);

		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JPanel panel = new JPanel();
		panel.setBounds(5, 5, 364, 289);
		panel.setLayout(null);
		panel.setBorder(BorderFactory.createTitledBorder("Watch List Selection"));
		contentPane.add(panel);

		JLabel lblSelectStockSymbol = new JLabel("Select Stock Symbols");
		lblSelectStockSymbol.setBounds(10, 21, 214, 14);
		panel.add(lblSelectStockSymbol);

		lstSymbols = new JList<String>();
		lstSymbols.setBounds(10, 37, 344, 241);
		lstSymbols.setModel(new DefaultComboBoxModel<String>(symbols));
		lstSymbols.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		lstSymbols.setVisibleRowCount(5);

		JScrollPane scrollPane = new JScrollPane(lstSymbols);
		scrollPane.setBounds(10, 39, 344, 239);
		panel.add(scrollPane);
		
		//Select indexes if already have
		try {
			StockController controller = new StockController();
			String symbolNames = controller.getWatchListSymbols();
			if (!symbolNames.contains("ERROR:")) {
				ArrayList<Integer> indexes = getSymbolIndexes(symbolNames);
				int[] selectedSymbols = indexes.stream().mapToInt(i -> i).toArray();
				lstSymbols.setSelectedIndices(selectedSymbols);
			}
		} catch (Exception ex) {
			errorMessage(ex.getMessage(), "Exception");
			clogger.logException(VIEW_NAME, ex);
		}

		JButton btnCreateWatchList = new JButton("Create Watch List");
		btnCreateWatchList.setBounds(15, 305, 344, 42);
		contentPane.add(btnCreateWatchList);
		btnCreateWatchList.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					int[] indexs = lstSymbols.getSelectedIndices();
					if (indexs.length > 0) {
						String symbolIndex = "";
						for (int i : indexs) {
							symbolIndex += i + ",";
						}
						symbolIndex = symbolIndex.trim().substring(0, symbolIndex.length() - 1); 
						ArrayList<String> symbolList = getSymbolNames(symbolIndex);
						String sysmbols = String.join(",", symbolList);
						// Save it
						StockController controller = new StockController();
						String status = controller.saveWatchListSymbols(sysmbols);
						if (!status.contains("ERROR:")) {
							infoMessage("you have selected following sysmbols : \r\n" + sysmbols, VIEW_NAME);
						} else {
							errorMessage(status, VIEW_NAME);
						}
					}

					else {
						errorMessage("Please select at-least one Symbol.", VIEW_NAME);
					}
				} catch (Exception ex) {
					errorMessage(ex.getMessage(), "Exception");
					clogger.logException(VIEW_NAME, ex);
				}

			}
		});
	}
}
