package com.stockAnalyzer.views;

import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.NumberFormat;
import java.util.ArrayList;

import javax.swing.AbstractAction;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.annotations.XYAnnotation;
import org.jfree.chart.annotations.XYDrawableAnnotation;
import org.jfree.chart.annotations.XYPointerAnnotation;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.labels.StandardXYToolTipGenerator;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.time.Day;
import org.jfree.data.xy.XYDataset;
import org.jfree.ui.TextAnchor;

import com.stockAnalyzer.models.StockDetail;

/**
 * This class helpful to showing how to create a line chart using data from an
 * {@link XYDataset}.
 * 
 * @author SajjadAshrafCan
 *
 */
public class AVGChart extends JFrame {

	private static final long serialVersionUID = 1L;
	private String chartTitle, xaxisLable, yaxisLable;
	private XYDataset dataset;
	private ChartPanel chartPanel;
	private ArrayList<StockDetail> stockAnnotation;

	/**
	 * Creates a new demo.
	 *
	 * @param title
	 *            the frame title.
	 */
	public AVGChart(String windowTitle, String chartTitle, String xaxisLable, String yaxisLable, XYDataset dataset,
			ArrayList<StockDetail> stockAnnotation) {
		super(windowTitle);

		setLayout(new BorderLayout(0, 5));

		this.chartTitle = chartTitle;
		this.xaxisLable = xaxisLable;
		this.yaxisLable = yaxisLable;
		this.dataset = dataset;
		this.stockAnnotation = stockAnnotation;

		JFreeChart chart = createChart();
		chartPanel = new ChartPanel(chart);

		add(chartPanel, BorderLayout.CENTER);
		chartPanel.setMouseWheelEnabled(true);
		chartPanel.setHorizontalAxisTrace(true);
		chartPanel.setVerticalAxisTrace(true);

		JPanel panel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
		panel.add(createTrace());
		panel.add(createDate());
		panel.add(createZoom());
		add(panel, BorderLayout.SOUTH);

	}

	/**
	 * Creates a chart.
	 * 
	 * @param dataset
	 *            the data for the chart.
	 * 
	 * @return a chart.
	 */
	private JFreeChart createChart() {

		JFreeChart chart = ChartFactory.createTimeSeriesChart(chartTitle, xaxisLable, yaxisLable, dataset, true, true,
				false);
		XYPlot plot = chart.getXYPlot();
		XYLineAndShapeRenderer renderer = (XYLineAndShapeRenderer) plot.getRenderer();
		renderer.setBaseShapesVisible(true);
		NumberFormat currency = NumberFormat.getCurrencyInstance();
		currency.setMaximumFractionDigits(0);
		NumberAxis rangeAxis = (NumberAxis) plot.getRangeAxis();
		rangeAxis.setNumberFormatOverride(currency);

		plot.getRenderer().setToolTipGenerator(StandardXYToolTipGenerator.getTimeSeriesInstance());
		// label the best bid with an arrow and label...
		for (StockDetail stockDetail : stockAnnotation) {
			double x = new Day(stockDetail.getDate()).getFirstMillisecond();
			final CircleDrawer cd = new CircleDrawer(Color.red, new BasicStroke(1.0f), null);
			Color TextColor = Color.blue;
			double y = 0.0;
			if (stockDetail.getDecision().equals("Buy")) {
				y = stockDetail.getAvgClose2();
				TextColor = Color.BLUE;
			} else {
				y = stockDetail.getAvgClose1();
				TextColor = Color.RED;
			}

			final XYAnnotation bestBid = new XYDrawableAnnotation(x, y, 11, 11, cd);
			plot.addAnnotation(bestBid);
			final XYPointerAnnotation pointer = new XYPointerAnnotation(stockDetail.getDecision(), x, y,
					3.0 * Math.PI / 4.0);
			pointer.setBaseRadius(35.0);
			pointer.setTipRadius(10.0);
			pointer.setFont(new Font("SansSerif", Font.PLAIN, 9));
			pointer.setPaint(TextColor);
			pointer.setTextAnchor(TextAnchor.HALF_ASCENT_RIGHT);
			plot.addAnnotation(pointer);
		}

		return chart;

	}

	/**
	 * Create trace.
	 * 
	 * @return
	 */
	private JComboBox createTrace() {
		final JComboBox trace = new JComboBox();
		final String[] traceCmds = { "Enable Trace", "Disable Trace" };
		trace.setModel(new DefaultComboBoxModel(traceCmds));
		trace.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (traceCmds[0].equals(trace.getSelectedItem())) {
					chartPanel.setHorizontalAxisTrace(true);
					chartPanel.setVerticalAxisTrace(true);
					chartPanel.repaint();
				} else {
					chartPanel.setHorizontalAxisTrace(false);
					chartPanel.setVerticalAxisTrace(false);
					chartPanel.repaint();
				}
			}
		});
		return trace;
	}

	/**
	 * Create date.
	 * 
	 * @return
	 */
	private JComboBox createDate() {
		final JComboBox date = new JComboBox();
		final String[] dateCmds = { "Horizontal Dates", "Vertical Dates" };
		date.setModel(new DefaultComboBoxModel(dateCmds));
		date.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				JFreeChart chart = chartPanel.getChart();
				XYPlot plot = (XYPlot) chart.getPlot();
				DateAxis domain = (DateAxis) plot.getDomainAxis();
				if (dateCmds[0].equals(date.getSelectedItem())) {
					domain.setVerticalTickLabels(false);
				} else {
					domain.setVerticalTickLabels(true);
				}
			}
		});
		return date;
	}

	/**
	 * Create zoom.
	 * 
	 * @return
	 */
	private JButton createZoom() {
		final JButton auto = new JButton(new AbstractAction("Auto Zoom") {

			@Override
			public void actionPerformed(ActionEvent e) {
				chartPanel.restoreAutoBounds();
			}
		});
		return auto;
	}

}
