package com.stockAnalyzer.views;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import com.stockAnalyzer.controller.StockController;

/**
 * Register screen
 * @author SajjadAshrafCan
 *
 */
public class Register extends BaseView {

	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField txtFullName;
	private JTextField txtUserName;
	private JTextField txtEmail;
	private JPasswordField txtPassword;
	private static String VIEW_NAME = Register.class.getName();

	/**
	 * Constructor
	 * Create the frame.
	 */
	public Register() {
		super(VIEW_NAME);

		setTitle("Register");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 424, 374);
		setResizable(false);
		centreWindow(this);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JPanel panel = new JPanel();
		panel.setBounds(10, 11, 383, 276);
		panel.setBorder(BorderFactory.createTitledBorder("Register"));
		contentPane.add(panel);
		panel.setLayout(null);

		JLabel lblFullName = new JLabel("Full Name");
		lblFullName.setBounds(19, 34, 58, 14);
		panel.add(lblFullName);

		txtFullName = new JTextField();
		txtFullName.setColumns(10);
		txtFullName.setBounds(19, 51, 345, 20);
		txtFullName.addKeyListener(new KeyAdapter() {
			public void keyTyped(KeyEvent e) {
				if (txtFullName.getText().length() >= 15)
					e.consume();
			}
		});
		panel.add(txtFullName);

		txtUserName = new JTextField();
		txtUserName.setColumns(10);
		txtUserName.setBounds(19, 110, 345, 20);
		txtUserName.addKeyListener(new KeyAdapter() {
			public void keyTyped(KeyEvent e) {
				if (txtUserName.getText().length() >= 15)
					e.consume();
			}
		});
		panel.add(txtUserName);

		JLabel label_1 = new JLabel("User Name");
		label_1.setBounds(19, 92, 102, 14);
		panel.add(label_1);

		txtEmail = new JTextField();
		txtEmail.setColumns(10);
		txtEmail.setBounds(19, 169, 345, 20);
		txtEmail.addKeyListener(new KeyAdapter() {
			public void keyTyped(KeyEvent e) {
				if (txtEmail.getText().length() >= 50)
					e.consume();
			}
		});
		panel.add(txtEmail);

		JLabel lblEmailAddress = new JLabel("Email Address");
		lblEmailAddress.setBounds(19, 154, 92, 14);
		panel.add(lblEmailAddress);

		txtPassword = new JPasswordField();
		txtPassword.setBounds(19, 228, 345, 20);
		txtPassword.addKeyListener(new KeyAdapter() {
			public void keyTyped(KeyEvent e) {
				if (String.valueOf(txtPassword.getPassword()).length() >= 15)
					e.consume();
			}
		});
		panel.add(txtPassword);

		JLabel label_3 = new JLabel("Password");
		label_3.setBounds(19, 213, 112, 14);
		panel.add(label_3);

		JButton btnRegister = new JButton("Register");
		btnRegister.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					// validate input
					String fullName = txtFullName.getText();
					String userName = txtUserName.getText();
					String email = txtEmail.getText();
					String password = String.valueOf(txtPassword.getPassword());

					if (userName.length() < 1) {
						infoMessage("Please enter user Name.", VIEW_NAME);
					} else if (password.length() < 1) {
						infoMessage("Please enter passsword.", VIEW_NAME);
					} else if (email.length() < 1) {
						infoMessage("Please enter Email Address.", VIEW_NAME);
					} else if (fullName.length() < 1) {
						infoMessage("Please enter Full Name.", VIEW_NAME);
					} else {
						StockController controller = new StockController();
						String status = controller.addUser(userName, email, fullName, password);
						if (status.equals("OK")) {
							successMessage("Account created Successfully!, Click Ok and Login now.", VIEW_NAME);
							moveToLogin(Register.this);
						} else {
							warningMessage("Error occurred in creating account, Reason:" + status + ".", VIEW_NAME);
						}
					}
				} catch (Exception ex) {
					errorMessage(ex.getMessage(), "Exception");
					clogger.logException(VIEW_NAME, ex);
				}
			}
		});
		btnRegister.setBounds(157, 298, 89, 23);
		contentPane.add(btnRegister);
	}
}
