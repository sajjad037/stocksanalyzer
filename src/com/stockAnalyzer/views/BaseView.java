package com.stockAnalyzer.views;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.logging.Logger;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import com.stockAnalyzer.utilities.CLogger;

/**
 * Base view have all necessary functions that is needed by majority of views. 
 * @author SajjadAshrafCan
 *
 */

public class BaseView extends JFrame {
	private static final long serialVersionUID = 1L;
	protected CLogger clogger;
	protected String symbols[] = { "AAPL - Apple", "AXP - American Express", "BA - Boeing", "CAT - Caterpillar",
			"CSCO - Cisco Systems", "CVX - Chevron", "KO - Coca-Cola", "DD - DuPont", "XOM - ExxonMobil",
			"GE - General Electric", "GS - Goldman Sachs", "HD - Home Depot", "IBM - IBM", "INTC - Intel",
			"JNJ - Johnson & Johnson", "JPM - JPMorgan Chase", "MCD - McDonald's", "MMM - 3M Company", "MRK - Merck",
			"MSFT - Microsoft", "NKE - Nike", "PFE - Pfizer", "PG - Procter & Gamble", "TRV - The Travelers",
			"UNH - UnitedHealth", "UTX - United Technologies", "V - Visa", "VZ - Verizon", "WMT - Wal-Mart",
			"DIS - Walt Disney" };
	/**
	 * Constructor.
	 * @param viewName
	 */
	public BaseView(String viewName) {
		clogger = new CLogger(Logger.getLogger(viewName));
		clogger.log(String.format("%s %s", viewName, "Initialized.."));
	}
	
	/**
	 * Set window to user's screen center position.
	 * @param frame
	 */
	protected void centreWindow(JFrame frame) {
		Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
		int x = (int) ((dimension.getWidth() - frame.getWidth()) / 2);
		int y = (int) ((dimension.getHeight() - frame.getHeight()) / 2);
		frame.setLocation(x, y);
	}
	
	/**
	 * Move to, move from source window to destination.
	 * @param currentWindow
	 * @param newWindow
	 * @param closeWindow
	 */
	protected void moveTo(JFrame currentWindow, JFrame newWindow, boolean closeWindow) {
		newWindow.setVisible(true);

		if (closeWindow) {
			currentWindow.dispose();
		}
	}

	/**
	 * Move to login screen.
	 * @param currentWindow
	 */
	protected void moveToLogin(JFrame currentWindow) {
		moveTo(currentWindow, new Login(), true);
	}

	protected void moveToWatchList(JFrame currentWindow) {
		moveTo(currentWindow, new StockWatchList(), false);
	}
	
	/**
	 * Move to stock rating screen.
	 * @param currentWindow
	 * @throws ParseException
	 */
	protected void moveToStockRating(JFrame currentWindow) throws ParseException {
		moveTo(currentWindow, new StockRating(), true);
	}
	
	/**
	 * Move to register screen.
	 * @param currentWindow
	 */
	protected void moveToRegister(JFrame currentWindow) {
		moveTo(currentWindow, new Register(), true);
	}
	
	/**
	 * Move to forget password screen.
	 * @param currentWindow
	 */
	protected void moveToForgetPassword(JFrame currentWindow) {
		moveTo(currentWindow, new ForgotPassword(), true);
	}
	
	/**
	 * Information message pop up. 
	 * @param infoMessage
	 * @param titleBar
	 */
	protected void infoMessage(String infoMessage, String titleBar) {
		JOptionPane.showMessageDialog(null, infoMessage, "Stock Analyzer: " + titleBar,
				JOptionPane.INFORMATION_MESSAGE);
	}
	
	/**
	 * Error message pop up.
	 * @param errorMessage
	 * @param titleBar
	 */
	protected static void errorMessage(String errorMessage, String titleBar) {
		JOptionPane.showMessageDialog(null, errorMessage, "Stock Analyzer: " + titleBar, JOptionPane.ERROR_MESSAGE);
	}

	/**
	 * Warning message pop up.
	 * @param warningMessage
	 * @param titleBar
	 */	
	protected void warningMessage(String warningMessage, String titleBar) {
		JOptionPane.showMessageDialog(null, warningMessage, "Stock Analyzer: " + titleBar, JOptionPane.WARNING_MESSAGE);
	}
	
	/**
	 * Success message pop up.
	 * @param successMessage
	 * @param titleBar
	 */
	protected void successMessage(String successMessage, String titleBar) {
		JOptionPane.showMessageDialog(null, successMessage, "Stock Analyzer: " + titleBar, JOptionPane.PLAIN_MESSAGE);
	}
	
	/**
	 * Get symbol indexes.
	 * @param symbolNames
	 * @return
	 */
	protected ArrayList<Integer> getSymbolIndexes(String symbolNames) {
		ArrayList<Integer> indexlist = new ArrayList<Integer>();
		String[] sybName = symbolNames.split(",");
		for (int i = 0; i < sybName.length; i++) {

			for (int j = 0; j < symbols.length; j++) {
				if (symbols[j].equals(sybName[i])) {
					indexlist.add(j);
					break;
				}
			}
		}
		return indexlist;
	}
	
	/**
	 * Get symbol names.
	 * @param symbolIndex
	 * @return
	 */
	protected ArrayList<String> getSymbolNames(String symbolIndex) {
		ArrayList<String> indexlist = new ArrayList<String>();
		String[] sybIndex = symbolIndex.split(",");
		for (int i = 0; i < sybIndex.length; i++) {
			int index = Integer.parseInt(sybIndex[i]);
			if (index < symbols.length) {
				indexlist.add(symbols[index]);
			}
		}
		return indexlist;
	}

}
