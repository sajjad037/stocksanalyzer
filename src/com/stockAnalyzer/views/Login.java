package com.stockAnalyzer.views;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.File;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;

import com.stockAnalyzer.controller.StockController;

/**
 * Login screen.
 * @author SajjadAshrafCan
 *
 */
public class Login extends BaseView {

	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField textField;
	private JPanel panel;
	private JLabel lblNewLabel;
	private JTextField txtUserName;
	private JLabel lblPassword;
	private JPasswordField txtPassword;
	private JButton btnRegister;
	private JButton btnLogin;
	private static String VIEW_NAME = Login.class.getName();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {

			public void run() {
				try {
					File file = new File(StockController.getDirectoryPath());
					if (file.isDirectory()) {

						Login frame = new Login();
						frame.setVisible(true);
					} else {
						errorMessage("First Create Following directory in C: Drive. \r\n "
								+ StockController.getDirectoryPath(), "Exception");
					}

				} catch (Exception e) {
					e.printStackTrace();
					errorMessage(e.getMessage(), "Exception");
				}
			}
		});
	}

	/**
	 * Constructor 
	 * Create the frame.
	 */
	public Login() {
		super(VIEW_NAME);

		setTitle("Login");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 401, 262);
		setResizable(false);
		centreWindow(this);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		textField = new JTextField();
		textField.setBounds(10, 66, 142, -19);
		contentPane.add(textField);
		textField.setColumns(10);

		panel = new JPanel();
		panel.setBorder(new LineBorder(new Color(0, 0, 0), 4, true));
		panel.setBorder(BorderFactory.createTitledBorder("Login"));
		panel.setBounds(10, 11, 365, 161);
		contentPane.add(panel);
		panel.setLayout(null);

		lblNewLabel = new JLabel("User Name");
		lblNewLabel.setBounds(10, 21, 97, 14);
		panel.add(lblNewLabel);

		txtUserName = new JTextField();
		txtUserName.setBounds(10, 37, 345, 20);
		panel.add(txtUserName);
		txtUserName.setColumns(10);
		txtUserName.addKeyListener(new KeyAdapter() {
			public void keyTyped(KeyEvent e) {
				if (txtUserName.getText().length() >= 15)
					e.consume();
			}
		});

		lblPassword = new JLabel("Password");
		lblPassword.setBounds(10, 68, 76, 14);
		panel.add(lblPassword);

		JCheckBox chRemeberMe = new JCheckBox("Remeber me");
		chRemeberMe.setBounds(10, 119, 117, 23);
		panel.add(chRemeberMe);

		txtPassword = new JPasswordField();
		txtPassword.setBounds(10, 82, 345, 20);
		panel.add(txtPassword);
		txtPassword.addKeyListener(new KeyAdapter() {
			public void keyTyped(KeyEvent e) {
				if (String.valueOf(txtPassword.getPassword()).length() >= 15)
					e.consume();
			}
		});

		JButton btnForgotPassword = new JButton("Forgot password ?");
		btnForgotPassword.setForeground(Color.BLUE);
		btnForgotPassword.setHorizontalAlignment(SwingConstants.RIGHT);
		btnForgotPassword.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				moveToForgetPassword(Login.this);
			}
		});
		btnForgotPassword.setBounds(189, 119, 166, 23);
		btnForgotPassword.setBorderPainted(false);
		btnForgotPassword.setOpaque(false);
		btnForgotPassword.setBackground(Color.lightGray);
		btnForgotPassword.setHorizontalAlignment(SwingConstants.RIGHT);
		Font buttonFont = new Font(btnForgotPassword.getFont().getName(), Font.CENTER_BASELINE + Font.BOLD,
				btnForgotPassword.getFont().getSize());
		btnForgotPassword.setFont(buttonFont);
		panel.add(btnForgotPassword);

		btnRegister = new JButton("Register");
		btnRegister.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				moveToRegister(Login.this);
			}
		});
		btnRegister.setBounds(20, 183, 89, 23);
		contentPane.add(btnRegister);

		btnLogin = new JButton("Login");
		btnLogin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					// validate input
					String userName = txtUserName.getText();
					String password = String.valueOf(txtPassword.getPassword());
					if (userName.length() < 1) {
						infoMessage("Please enter user Name.", VIEW_NAME);
					} else if (password.length() < 1) {
						infoMessage("Please enter passsword.", VIEW_NAME);
					} else {
						StockController controller = new StockController();
						String status = controller.validateUser(userName, password);
						if (status.contains("OK")) {
							moveToStockRating(Login.this);
						} else {
							warningMessage(status, VIEW_NAME);
						}
					}
				} catch (Exception ex) {
					clogger.logException(VIEW_NAME, ex);
					errorMessage(ex.getMessage(), "Exception");
				}

			}
		});
		btnLogin.setBounds(276, 183, 89, 23);
		contentPane.add(btnLogin);
	}
}
