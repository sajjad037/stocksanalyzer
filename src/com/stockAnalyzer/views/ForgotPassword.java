package com.stockAnalyzer.views;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import com.stockAnalyzer.controller.StockController;

/**
 * Forgot password screen
 * @author SajjadAshrafCan
 *
 */
public class ForgotPassword extends BaseView {
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField txtEmail;
	private static String VIEW_NAME = ForgotPassword.class.getName();

	/**
	 * Constructor.
	 * Create frame.
	 */
	public ForgotPassword() {
		super(VIEW_NAME);

		setTitle("Forgot Password");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 415, 216);
		setResizable(false);
		centreWindow(this);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JPanel panel = new JPanel();
		panel.setBounds(10, 11, 379, 103);
		panel.setBorder(BorderFactory.createTitledBorder("Forgot Password"));
		contentPane.add(panel);
		panel.setLayout(null);

		JLabel lblNewLabel = new JLabel("Email Address");
		lblNewLabel.setBounds(20, 33, 96, 14);
		panel.add(lblNewLabel);

		txtEmail = new JTextField();
		txtEmail.setBounds(20, 53, 338, 20);
		txtEmail.addKeyListener(new KeyAdapter() {
			public void keyTyped(KeyEvent e) {
				if (txtEmail.getText().length() >= 50)
					e.consume();
			}
		});
		panel.add(txtEmail);
		txtEmail.setColumns(10);

		JButton btnSend = new JButton("Send");
		btnSend.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					String email = txtEmail.getText();
					if (email.length() < 1) {
						infoMessage("Please enter Email Address.", VIEW_NAME);
					} else {
						StockController controller = new StockController();
						String status = controller.forgotPasswordEmail(email);
						if (status.contains("OK")) {
							successMessage(status.replace("OK:", "") + "\r\n Click Ok and Login now.", VIEW_NAME);
							moveToLogin(ForgotPassword.this);
						} else {
							warningMessage(status, VIEW_NAME);
						}
					}
				} catch (Exception ex) {
					errorMessage(ex.getMessage(), "Exception");
					clogger.logException(VIEW_NAME, ex);
				}
			}
		});
		btnSend.setBounds(155, 130, 89, 23);
		contentPane.add(btnSend);
	}

}
