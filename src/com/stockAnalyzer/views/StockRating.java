package com.stockAnalyzer.views;

import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.Timer;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.text.MaskFormatter;

import org.jfree.data.xy.XYDataset;
import org.jfree.ui.RefineryUtilities;

import com.stockAnalyzer.controller.StockController;
import com.stockAnalyzer.models.StockDetail;
import com.stockAnalyzer.models.StockPrice;

/**
 * Stock rating screen.
 * 
 * @author SajjadAshrafCan
 *
 */
public class StockRating extends BaseView implements ActionListener {
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField txtFromDate;
	private JTextField txtToDate;
	private JLabel lbSymbolCompanyName, lbBid, lbAsk, lbOpen, lbPreviousClose, lbLast;
	private JComboBox cmbSymbol;
	private static String VIEW_NAME = StockRating.class.getName();
	private MaskFormatter maskFormatter;
	private JTable tblStockData;
	private DefaultTableModel model;
	private JLabel lbWatchListResults;
	private static final int RATE = 10;
	private final Timer timer = new Timer(1000 / RATE, this);
	private int index;
	private String s;
	private int n;

	/**
	 * Constructor. Create the frame.
	 * 
	 * @throws ParseException
	 */
	public StockRating() throws ParseException {
		super(VIEW_NAME);
		maskFormatter = new MaskFormatter("##/##/####");

		setTitle("Stock Rating");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 653, 653);
		setResizable(false);
		centreWindow(this);

		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);

		JMenu fileMenu = new JMenu("File");
		menuBar.add(fileMenu);
		JMenuItem menuItemWatchList = new JMenuItem("WatchList", KeyEvent.VK_N);
		JMenuItem menuItemLogout = new JMenuItem("Logout", KeyEvent.VK_N);
		JMenuItem menuItemClose = new JMenuItem("Close", KeyEvent.VK_N);

		menuItemClose.setMnemonic(KeyEvent.VK_E);
		menuItemClose.setToolTipText("Exit application.");
		menuItemClose.addActionListener((ActionEvent event) -> {
			System.exit(0);
		});

		menuItemWatchList.setMnemonic(KeyEvent.VK_E);
		menuItemWatchList.setToolTipText("Add stock symbol for watch list.");
		menuItemWatchList.addActionListener((ActionEvent event) -> {
			moveToWatchList(StockRating.this);
		});

		menuItemLogout.setMnemonic(KeyEvent.VK_E);
		menuItemLogout.setToolTipText("Logout, Move to Login Screen.");
		menuItemLogout.addActionListener((ActionEvent event) -> {
			moveToLogin(StockRating.this);
		});

		fileMenu.add(menuItemWatchList);
		fileMenu.add(menuItemLogout);
		fileMenu.add(menuItemClose);

		JMenu helpMenu = new JMenu("Help");
		menuBar.add(helpMenu);
		JMenuItem menuItemAbout = new JMenuItem("About", KeyEvent.VK_N);

		menuItemAbout.setMnemonic(KeyEvent.VK_E);
		menuItemAbout.setToolTipText("Information about Application versions and developers.");
		menuItemAbout.addActionListener((ActionEvent event) -> {
			infoMessage("Stock Analyzer 2.0 \r\n Created by Group I.", VIEW_NAME);
		});

		helpMenu.add(menuItemAbout);

		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JPanel panel = new JPanel();
		panel.setBounds(18, 10, 603, 115);
		panel.setBorder(BorderFactory.createTitledBorder("Search"));
		contentPane.add(panel);
		panel.setLayout(null);

		JLabel lblFromDate = new JLabel("From Date");
		lblFromDate.setBounds(228, 15, 71, 14);
		panel.add(lblFromDate);

		// txtFromDate = new JTextField();
		txtFromDate = new JFormattedTextField(maskFormatter);
		txtFromDate.setColumns(10);
		txtFromDate.setBounds(228, 30, 147, 20);
		panel.add(txtFromDate);

		JLabel lblToDate = new JLabel("To Date");
		lblToDate.setBounds(428, 15, 71, 14);
		panel.add(lblToDate);

		txtToDate = new JFormattedTextField(maskFormatter);
		txtToDate.setColumns(10);
		txtToDate.setBounds(428, 30, 147, 20);
		panel.add(txtToDate);

		JLabel lblRang = new JLabel("Range 1 (Short)");
		lblRang.setBounds(40, 61, 89, 14);
		panel.add(lblRang);

		cmbSymbol = new JComboBox();
		cmbSymbol.setModel(new DefaultComboBoxModel(symbols));
		cmbSymbol.setBounds(40, 36, 147, 20);
		cmbSymbol.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				loadCurrentPrice();
			}
		});
		AutoCompletion.enable(cmbSymbol);
		panel.add(cmbSymbol);

		JComboBox cmbRange1 = new JComboBox();
		cmbRange1.setModel(new DefaultComboBoxModel(new String[] { "10", "20", "50", "100", "200" }));
		cmbRange1.setBounds(40, 78, 147, 20);
		panel.add(cmbRange1);

		JComboBox cmbRange2 = new JComboBox();
		cmbRange2.setModel(new DefaultComboBoxModel(new String[] { "10", "20", "50", "100", "200" }));
		cmbRange2.setBounds(228, 78, 147, 20);
		panel.add(cmbRange2);

		JLabel lblRang_1 = new JLabel("Range 2 (Long)");
		lblRang_1.setBounds(228, 61, 89, 14);
		panel.add(lblRang_1);

		JPanel graphPanel = new JPanel();
		graphPanel.setBounds(18, 260, 603, 269);
		graphPanel.setBorder(BorderFactory.createTitledBorder("Stock Data"));
		contentPane.add(graphPanel);
		model = new DefaultTableModel();
		graphPanel.setLayout(null);
		JScrollPane scrollPane = new JScrollPane(tblStockData);
		scrollPane.setBounds(10, 21, 583, 237);

		graphPanel.add(scrollPane);

		tblStockData = new JTable();
		scrollPane.setViewportView(tblStockData);
		tblStockData.setBorder(UIManager.getBorder("ToolTip.border"));
		tblStockData.setCellSelectionEnabled(true);
		tblStockData.setColumnSelectionAllowed(true);
		tblStockData.setModel(model);
		tblStockData.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
		tblStockData.setFillsViewportHeight(true);

		JButton btnSearch = new JButton("Search");
		btnSearch.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					//String symbol = cmbSymbol.getSelectedItem().toString();
					String symbol = cmbSymbol.getSelectedItem().toString().split(" - ")[0].trim();
					String strFromDate = txtFromDate.getText();
					String strToDate = txtToDate.getText();
					int range1 = Integer.parseInt(cmbRange1.getSelectedItem().toString());
					int range2 = Integer.parseInt(cmbRange2.getSelectedItem().toString());

					if (symbol.length() < 1) {
						infoMessage("Please select a symbol.", VIEW_NAME);
					} else if (strFromDate.length() < 1 || strFromDate.equals("  /  /    ")) {
						infoMessage("Please enter From date (MM/dd/yyyy).", VIEW_NAME);
					} else if (strToDate.length() < 1 || strToDate.equals("  /  /    ")) {
						infoMessage("Please enter To date (MM/dd/yyyy).", VIEW_NAME);
					} else if (range1 == range2) {
						infoMessage("Range1 and Range2 cannot be same.", VIEW_NAME);
					} else {

						EventQueue.invokeLater(new Runnable() {

							@Override
							public void run() {
								try {

									StockController controller = new StockController();

									StringBuilder msg = new StringBuilder();
									ArrayList<StockDetail> stockDetail = controller.getStockDetails(symbol, strFromDate,
											strToDate, range1, range2, msg);

									if (msg.toString().equals("OK")) {
										if (stockDetail != null && stockDetail.size() > 0) {
											String[] columns = { "S. #", "Date", "Close",
													"Short AVG(" + range1 + " Days)", "Long AVG (" + range2 + " Days)",
													"Decision" };
											model.setColumnIdentifiers(columns);

											DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
											ArrayList<StockDetail> stockAnnotation = new ArrayList<StockDetail>();
											for (int i = 0; i < stockDetail.size(); i++) {

												model.addRow(new Object[] { (i + 1),
														dateFormat.format(stockDetail.get(i).getDate()),
														stockDetail.get(i).getClose(),
														stockDetail.get(i).getAvgClose1(),
														stockDetail.get(i).getAvgClose2(),
														stockDetail.get(i).getDecision() });

												if (stockDetail.get(i).getDecision() != null
														&& !stockDetail.get(i).getDecision().equals(" - ")) {
													stockAnnotation.add(stockDetail.get(i));
												}
											}

											// Set Column Width
											tblStockData.getColumnModel().getColumn(0).setPreferredWidth(5);
											tblStockData.getColumnModel().getColumn(1).setPreferredWidth(10);
											tblStockData.getColumnModel().getColumn(2).setPreferredWidth(15);
											tblStockData.getColumnModel().getColumn(5).setPreferredWidth(5);

											XYDataset graphDatase = controller.createDataset(stockDetail, range1,
													range2);

											JFrame demo = new AVGChart("STOCK RATING GRAPH", "Stock Prediction Chart",
													"Date", "Value", graphDatase, stockAnnotation);
											demo.pack();
											RefineryUtilities.centerFrameOnScreen(demo);
											demo.setExtendedState(JFrame.MAXIMIZED_BOTH);
											demo.setLocationRelativeTo(null);
											demo.setVisible(true);
											demo.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

										} else {
											errorMessage("No Data found in this date rang.", VIEW_NAME);
										}
									} else {

										infoMessage(msg.toString(), VIEW_NAME);
									}
								} catch (Exception ex) {

								}
							}
						});
					}
				} catch (Exception ex) {
					errorMessage(ex.getMessage(), "Exception");
					clogger.logException(VIEW_NAME, ex);
				}
			}
		});
		btnSearch.setBounds(428, 61, 147, 37);
		panel.add(btnSearch);

		JLabel lblSelectASymbol = new JLabel("Select a Symbol");
		lblSelectASymbol.setBounds(40, 15, 131, 14);
		panel.add(lblSelectASymbol);

		JPanel currentStatusPanel = new JPanel();
		currentStatusPanel.setLayout(null);
		currentStatusPanel.setBorder(BorderFactory.createTitledBorder("Current Market Status"));
		currentStatusPanel.setBounds(18, 130, 603, 128);
		contentPane.add(currentStatusPanel);

		JLabel lblSymbol = new JLabel("Symbol - Company Name");
		lblSymbol.setBounds(40, 21, 152, 14);
		currentStatusPanel.add(lblSymbol);

		JLabel lblCompanyName = new JLabel("Bid");
		lblCompanyName.setBounds(228, 21, 89, 14);
		currentStatusPanel.add(lblCompanyName);

		JLabel lblAsk = new JLabel("Ask");
		lblAsk.setBounds(428, 21, 89, 14);
		currentStatusPanel.add(lblAsk);

		JLabel lblOpen = new JLabel("Open");
		lblOpen.setBounds(40, 71, 89, 14);
		currentStatusPanel.add(lblOpen);

		JLabel lblPreviousclose = new JLabel("PreviousClose");
		lblPreviousclose.setBounds(228, 71, 89, 14);
		currentStatusPanel.add(lblPreviousclose);

		JLabel lblLast = new JLabel("Last");
		lblLast.setBounds(428, 71, 89, 14);
		currentStatusPanel.add(lblLast);

		lbSymbolCompanyName = new JLabel("");
		lbSymbolCompanyName.setBounds(40, 35, 152, 25);
		lbSymbolCompanyName.setFont(new Font("Serif", Font.BOLD, 20));
		currentStatusPanel.add(lbSymbolCompanyName);

		lbBid = new JLabel("");
		lbBid.setBounds(228, 35, 152, 25);
		lbBid.setFont(new Font("Serif", Font.BOLD, 20));
		currentStatusPanel.add(lbBid);

		lbAsk = new JLabel("");
		lbAsk.setBounds(428, 35, 152, 25);
		lbAsk.setFont(new Font("Serif", Font.BOLD, 20));
		currentStatusPanel.add(lbAsk);

		lbOpen = new JLabel("");
		lbOpen.setBounds(40, 87, 152, 25);
		lbOpen.setFont(new Font("Serif", Font.BOLD, 20));
		currentStatusPanel.add(lbOpen);

		lbPreviousClose = new JLabel("");
		lbPreviousClose.setBounds(228, 87, 152, 25);
		lbPreviousClose.setFont(new Font("Serif", Font.BOLD, 20));
		currentStatusPanel.add(lbPreviousClose);

		lbLast = new JLabel("");
		lbLast.setBounds(428, 87, 152, 25);
		lbLast.setFont(new Font("Serif", Font.BOLD, 20));
		currentStatusPanel.add(lbLast);

		JPanel whatListPanel = new JPanel();
		whatListPanel.setLayout(null);
		whatListPanel.setBorder(BorderFactory.createTitledBorder("Watch List Status"));
		whatListPanel.setBounds(18, 540, 603, 52);
		contentPane.add(whatListPanel);

		EventQueue.invokeLater(new Runnable() {

			@Override
			public void run() {

				StringBuilder watchListResult = new StringBuilder();
				try {
					watchListResult.append(new StockController().getWatchListData());
				} catch (Exception e) {
					watchListResult.append(e.getMessage());
					e.printStackTrace();
					clogger.logException(VIEW_NAME, e);
				}
				s = watchListResult.toString();
				n = 100;
				lbWatchListResults = new JLabel(watchListResult.toString());
				lbWatchListResults.setBounds(10, 13, 583, 25);
				StringBuilder sb = new StringBuilder(n);
				for (int i = 0; i < n; i++) {
					sb.append(' ');
				}
				s = sb + s + sb;
				lbWatchListResults.setFont(new Font("Serif", Font.ITALIC, 20));
				lbWatchListResults.setText(sb.toString());
				whatListPanel.add(lbWatchListResults);
				timer.start();

			}
		});

		loadCurrentPrice();
	}

	private void loadCurrentPrice() {
		try {
			String symbol = cmbSymbol.getSelectedItem().toString();
			symbol = symbol.split(" - ")[0].trim();
			StockController controller = new StockController();
			StockPrice latestPrice = controller.getCurrentStockPrice(symbol);
			if (latestPrice != null) {
				String name = latestPrice.getSymbol() + " - " + latestPrice.getName();
				lbSymbolCompanyName.setText(name);
				lbSymbolCompanyName.setToolTipText(name);

				lbBid.setText(latestPrice.getBid() == -0.0 ? "N/A" : "" + latestPrice.getBid());
				lbAsk.setText(latestPrice.getAsk() == -0.0 ? "N/A" : "" + latestPrice.getAsk());
				lbOpen.setText(latestPrice.getOpen() == -0.0 ? "N/A" : "" + latestPrice.getOpen());
				lbPreviousClose
						.setText(latestPrice.getPreviousClose() == -0.0 ? "N/A" : "" + latestPrice.getPreviousClose());
				lbLast.setText(latestPrice.getLast() == -0.0 ? "N/A" : "" + latestPrice.getLast());

			}
		} catch (Exception ex) {
			errorMessage(ex.getMessage(), "Exception");
			clogger.logException(VIEW_NAME, ex);
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		index++;
		if (index > s.length() - n) {
			index = 0;
		}
		String txt = s.substring(index, index + n);
		lbWatchListResults.setText(txt);
	}
}
