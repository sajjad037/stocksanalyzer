package com.stockAnaluzer.persistance;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;

import com.stockAnalyzer.models.StockDetail;
import com.stockAnalyzer.models.StockPrice;

public interface IBaselFinanceAPI {
	public StockPrice getCurrentStockPrice(String symbolName) throws IOException;

	public ArrayList<StockDetail> getStockDetail(String symbolName, Date fromDate, Date toDate, int maxRange)
			throws IOException, NumberFormatException, ParseException;
}
