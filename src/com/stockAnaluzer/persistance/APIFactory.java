package com.stockAnaluzer.persistance;

import com.stockAnalyzer.utilities.Enums.FinanceAPI;

public class APIFactory {
	String dateFormat ="";
	public APIFactory(String dateFormat){
		this.dateFormat = dateFormat;
	}
	public IBaselFinanceAPI getAPI(FinanceAPI api) {
		switch (api) {
		case Yahoo:
			return new YahooFinanceAPI(this.dateFormat);

		case Xignite:
		default:
			return null;
		}
	}

}
