package com.stockAnaluzer.persistance;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.apache.commons.codec.binary.Base64;

import com.google.gson.Gson;

/**
 * This class is for saving a map
 * 
 * @author Sajjad Ashraf
 * 
 */
public class FileStorage {
	/**
	 * 
	 * Save file takes input file path and fileContent as string
	 * @param path
	 * @param fileContent
	 * @return
	 */
	public String saveTxtFile(String path, String fileContent) {
		String status = "";
		try {
			FileWriter fileWriter = new FileWriter(path);
			fileWriter.write(fileContent);
			fileWriter.flush();
			fileWriter.close();
			status = "OK";
		} catch (Exception e) {
			e.printStackTrace();
			status = e.getMessage();
		}
		return status;
	}
	/**
	 * Read text file from path
	 * @param path
	 * @return
	 * @throws IOException
	 */
	public String readTxtFile(String path) throws IOException {
		return new String(Files.readAllBytes(Paths.get(path)));
	}

	/**
	 * this method gets json from object
	 * 
	 * @param new_object
	 *            new object
	 * @return json converts gson to json and returns it
	 */
	public String getJsonFromObject(Object new_object) {
		Gson gson = new Gson();
		return gson.toJson(new_object);
	}

	/**
	 * this methods gets object from a json
	 * 
	 * @param new_jsonString
	 *            json string object
	 * @param new_class
	 *            new clas
	 * @return object object from json
	 */
	public Object getObjectFromJson(String new_jsonString, Class<?> new_class) {
		Gson gson = new Gson();
		return gson.fromJson(new_jsonString, new_class);
	}

	/**
	 * this method encodes the input to base64
	 * 
	 * @param new_input
	 *            string
	 * @return returns encoded string
	 */
	public String encodeBase64(String new_input) {
		byte[] bytesEncoded = Base64.encodeBase64(new_input.getBytes());
		return new String(bytesEncoded);

	}

	/**
	 * this method decodes the string from base64 encoding
	 * 
	 * @param new_input
	 *            string base64
	 * @return string the result
	 */
	public String decodeBase64(String new_input) {
		// Decrypt data on other side, by processing encoded data
		byte[] valueDecoded = Base64.decodeBase64(new_input.getBytes());
		return new String(valueDecoded);

	}

}
