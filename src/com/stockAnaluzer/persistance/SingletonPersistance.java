package com.stockAnaluzer.persistance;

import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import com.stockAnalyzer.models.StockDetail;
import com.stockAnalyzer.models.UserInfo;

/**
 * SingleTon Pattern
 * 
 * @author SajjadAshrafCan
 *
 */
public class SingletonPersistance {

	ArrayList<UserInfo> usersList;
	private static SingletonPersistance instance;
	FileStorage filestorage;
	File userDataFile, csvFile, watchListFile;
	ArrayList<StockDetail> listOfStocks = null;
	String userInfoDateFilePath, csvFilePath, watchListFilePath, dateFormat = "";
	private DecimalFormat decimalFormat;
	
	/**
	 * Constructor. 
	 * @param userInfoDateFilePath
	 * @param csvFilePath
	 * @param watchListFilePath
	 * @param dateFormat
	 * @throws Exception
	 */
	private SingletonPersistance(String userInfoDateFilePath, String csvFilePath, String watchListFilePath,
			String dateFormat) throws Exception {

		this.userInfoDateFilePath = userInfoDateFilePath;
		this.csvFilePath = csvFilePath;
		this.watchListFilePath = watchListFilePath;
		this.dateFormat = dateFormat;

		filestorage = new FileStorage();
		usersList = new ArrayList<UserInfo>();
		listOfStocks = new ArrayList<StockDetail>();
		userDataFile = new File(this.userInfoDateFilePath);
		csvFile = new File(this.csvFilePath);
		watchListFile = new File(this.watchListFilePath);
		decimalFormat = new DecimalFormat("#.##");

		loadUsers();
		// loadAllCsvData();
	}

	/**
	 * Get instance.
	 * @param userInfoDateFilePath
	 * @param csvFilePath
	 * @param watchListFilePath
	 * @param dateFormat
	 * @return
	 * @throws Exception
	 */
	public static SingletonPersistance getInstace(String userInfoDateFilePath, String csvFilePath,
			String watchListFilePath, String dateFormat) throws Exception {
		if (instance == null) {
			instance = new SingletonPersistance(userInfoDateFilePath, csvFilePath, watchListFilePath, dateFormat);
		}
		return instance;
	}

	/**
	 * Load Users in variable from disk.
	 */
	private void loadUsers() {
		try {
			if (userDataFile.exists()) {
				String fileContent = "";
				fileContent = filestorage.readTxtFile(userDataFile.getPath());
				fileContent = filestorage.decodeBase64(fileContent);
				UserInfo[] userInfoArray = (UserInfo[]) filestorage.getObjectFromJson(fileContent, UserInfo[].class);
				if (userInfoArray != null) {
					usersList = new ArrayList<>(Arrays.asList(userInfoArray));
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
			usersList = null;
		}

		if (usersList == null) {
			usersList = new ArrayList<UserInfo>();
		}

	}

	/**
	 * User Credential are valid or not.
	 * @param user
	 * @return
	 */
	public boolean validUser(UserInfo user) {
		boolean status = false;
		if (usersList != null && usersList.size() > 0) {
			for (UserInfo userInfo : usersList) {
				if (userInfo.getUserName().equalsIgnoreCase(user.getUserName())) {
					if (userInfo.getPassword().equals(user.getPassword())) {
						status = true;
						user = userInfo;
						break;
					}
				}
			}
		}
		return status;
	}

	/**
	 * Check UserName or Email Address already exist or not.
	 * @param user
	 * @param propertyName
	 * @return
	 */
	public boolean hasALready(UserInfo user, String propertyName) {
		boolean status = false;
		if (usersList != null && usersList.size() > 0) {
			if (propertyName.equalsIgnoreCase("Email")) {
				for (UserInfo userInfo : usersList) {
					if (userInfo.getEmail().equals(user.getEmail())) {
						status = true;
						break;
					}
				}
			} else if (propertyName.equalsIgnoreCase("UserName")) {
				for (UserInfo userInfo : usersList) {
					if (userInfo.getUserName().equalsIgnoreCase(user.getUserName())) {
						status = true;
						break;
					}
				}
			}
		}
		return status;
	}

	/**
	 * Get User information via Email Address
	 * @param email
	 * @return
	 */
	public UserInfo getUserInfo(String email) {
		UserInfo user = null;
		for (UserInfo userInfo : usersList) {
			if (userInfo.getEmail().equals(email)) {
				user = userInfo;
				break;
			}
		}
		return user;
	}
	
	/**
	 * Add new User
	 * @param user
	 * @return
	 */
	public String addUser(UserInfo user) {
		usersList.add(user);
		String status = "";
		// if (userDataFile.exists()) {
		String fileContent = filestorage.getJsonFromObject(usersList);
		fileContent = filestorage.encodeBase64(fileContent);
		status = filestorage.saveTxtFile(userDataFile.getPath(), fileContent);
		loadUsers();
		// } else {
		// status = "User Data file not exists.";
		// }
		return status;
	}

	/**
	 * Set Stock List
	 * @param stockList
	 */
	public void setListOfStocks(ArrayList<StockDetail> stockList) {
		listOfStocks = stockList;
		// Collections.sort(listOfStocks, Collections.reverseOrder());
		Collections.sort(listOfStocks, new Comparator<StockDetail>() {
			@Override
			public int compare(StockDetail o1, StockDetail o2) {
				return o1.getDate().compareTo(o2.getDate());
			}
		});
	}

	/**
	 * Get Stock Data between Date range
	 * @param fromDate
	 * @param toDate
	 * @return
	 * @throws Exception
	 */
	public ArrayList<StockDetail> getStockData(Date fromDate, Date toDate) throws Exception {
		if (this.listOfStocks != null && this.listOfStocks.size() > 0) {
			Date date = null;
			ArrayList<StockDetail> stockList = new ArrayList<StockDetail>();
			for (StockDetail stockDetail : this.listOfStocks) {
				date = stockDetail.getDate();
				if (date.after(fromDate) && date.before(toDate)) {
					stockList.add(stockDetail);
				} else if (date.equals(fromDate)) {
					stockList.add(stockDetail);
				} else if (date.equals(toDate)) {
					stockList.add(stockDetail);
				}
			}
			return stockList.size() == 0 ? null : stockList;
		} else {
			return null;
		}
	}
	
	/**
	 * Get Stock data from Range Average
	 * @param stockList
	 * @param range
	 * @param rangeName
	 * @return
	 * @throws Exception
	 */
	public ArrayList<StockDetail> getStockRangeData(ArrayList<StockDetail> stockList, int range, String rangeName)
			throws Exception {

		// get intial Index
		int initialIndex = listOfStocks.indexOf(stockList.get(0));
		int maxAvailable = listOfStocks.size() - initialIndex;
		List<Integer> indexsToremove = new ArrayList<Integer>();
		if (range > maxAvailable) {
			range = maxAvailable;
		}

		double avgClosingRate = 0.0;
		// ArrayList<StockDetail> rangeList = new ArrayList<StockDetail>();
		for (StockDetail stockDetail : stockList) {
			boolean rangeOverFlow = initialIndex + range > listOfStocks.size();
			if (!rangeOverFlow) {

				for (int i = 0; i < range; i++) {
					avgClosingRate += listOfStocks.get(initialIndex + i).getClose();
				}

				avgClosingRate = avgClosingRate / range;
				avgClosingRate = Double.valueOf(decimalFormat.format(avgClosingRate));

				if (rangeName.equalsIgnoreCase("Range1")) {
					stockDetail.setAvgClose1(avgClosingRate);
				} else {
					stockDetail.setAvgClose2(avgClosingRate);
				}

				++initialIndex;
			}

			else {
				// remove item to list
				int index = stockList.indexOf(stockDetail);
				indexsToremove.add(index);
			}

		}

		// check to remove index
		if (indexsToremove.size() > 0) {
			Collections.sort(indexsToremove);
			Collections.reverse(indexsToremove);
			for (Integer index : indexsToremove) {
				stockList.remove(index.intValue());
			}

		}
		return stockList.size() == 0 ? null : stockList;

	}
	
	/**
	 * Save watch List information
	 * @param data
	 * @return
	 */
	public String saveWatchList(String data) {
		String status = "";
		try {
			// if (watchListFile.exists()) {
			String fileContent = filestorage.encodeBase64(data);
			status = filestorage.saveTxtFile(watchListFile.getPath(), fileContent);
			// } else {
			// status = "ERROR:Watch List File is not exist.";
			// }
		} catch (Exception e) {
			e.printStackTrace();
			status = "ERROR:" + e.getMessage();
		}
		return status;
	}

	/**
	 * Get watch List infromation
	 * @return
	 */
	public String getWatchList() {
		String fileContent = "";
		try {
			if (watchListFile.exists()) {
				fileContent = filestorage.readTxtFile(watchListFile.getPath());
				fileContent = filestorage.decodeBase64(fileContent);
			} else {
				fileContent = "ERROR:Watch List File is not exist.";
			}
		} catch (Exception e) {
			e.printStackTrace();
			fileContent = "ERROR:" + e.getMessage();
		}
		return fileContent;
	}
}
