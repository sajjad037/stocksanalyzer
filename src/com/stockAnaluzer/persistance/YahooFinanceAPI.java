package com.stockAnaluzer.persistance;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import com.stockAnalyzer.models.StockDetail;
import com.stockAnalyzer.models.StockPrice;
/**
 * Yahoo Finance API
 * This class call Yahoo Finance API to get Stock history data
 * @author SajjadAshrafCan
 *
 */
public class YahooFinanceAPI implements IBaselFinanceAPI {

	String currentStockPriceURL = "http://finance.yahoo.com/d/quotes.csv?s=%s&f=snbaopl1";
	String historicalStockDetailURL = "http://ichart.finance.yahoo.com/table.csv?s=%s&a=%d&b=%d&c=%d&d=%d&e=%d&f=%d&g=d&ignore=.csv";
	private final int CONNECTION_TIMEOUT = 10000;
	private final String QUOTES_CSV_DELIMITER = ",";
	private String dateFormat = "";
	private DecimalFormat decimalFormat;
	
	/**
	 * Constructor 
	 * @param dateFormat
	 */
	public YahooFinanceAPI(String dateFormat) {
		this.dateFormat = dateFormat;
		decimalFormat = new DecimalFormat("#.##");
	}
	
	/**
	 * Get Stock Current Price
	 * @param symbolName
	 * @return
	 * @throws IOException
	 */
	public StockPrice getCurrentStockPrice(String symbolName) throws IOException {
		String url = String.format(currentStockPriceURL, symbolName);
		StockPrice price = null;

		URL request = new URL(url);

		URLConnection connection = request.openConnection();
		connection.setConnectTimeout(CONNECTION_TIMEOUT);
		connection.setReadTimeout(CONNECTION_TIMEOUT);
		InputStreamReader is = new InputStreamReader(connection.getInputStream());
		BufferedReader br = new BufferedReader(is);
		// br.readLine(); // skip the first line
		// Parse CSV
		for (String line = br.readLine(); line != null; line = br.readLine()) {
			line = line.replace("N/A", "-0.000000");
			line = line.replace(", Inc.", " Inc.");
			line = line.replace(", Inc", " Inc.");
			price = praseStockPrice(line);
		}

		return price;
	}

	/**
	 * Parse data of  Stock Current Price
	 * @param line
	 * @return
	 */
	private StockPrice praseStockPrice(String line) {
		String[] data = line.split(QUOTES_CSV_DELIMITER);
		return new StockPrice(data[0], data[1], Double.parseDouble(data[2]), Double.parseDouble(data[3]),
				Double.parseDouble(data[4]), Double.parseDouble(data[5]), Double.parseDouble(data[6]));
	}
	
	/**
	 * Get Stock History data
	 * @param symbolName
	 * @param fromDate
	 * @param toDate
	 * @param maxRange
	 * @return
	 * @throws IOException
	 * @throws NumberFormatException
	 * @throws ParseException
	 */
	public ArrayList<StockDetail> getStockDetail(String symbolName, Date fromDate, Date toDate, int maxRange)
			throws IOException, NumberFormatException, ParseException {
		ArrayList<StockDetail> stockList = new ArrayList<StockDetail>();

		Calendar from = Calendar.getInstance();
		Calendar to = Calendar.getInstance();

		from.setTime(fromDate);
		toDate = addBusinessDays(toDate, maxRange);
		to.setTime(toDate);

		String url = String.format(historicalStockDetailURL, symbolName, from.get(Calendar.MONTH),
				from.get(Calendar.DAY_OF_MONTH), from.get(Calendar.YEAR), to.get(Calendar.MONTH),
				to.get(Calendar.DAY_OF_MONTH), to.get(Calendar.YEAR));
		URL request = new URL(url);

		URLConnection connection = request.openConnection();
		connection.setConnectTimeout(CONNECTION_TIMEOUT);
		connection.setReadTimeout(CONNECTION_TIMEOUT);
		InputStreamReader is = new InputStreamReader(connection.getInputStream());
		BufferedReader br = new BufferedReader(is);
		br.readLine(); // skip the first line
		// Parse CSV
		for (String line = br.readLine(); line != null; line = br.readLine()) {
			stockList.add(parseStockDetail(symbolName, line));
		}

		return stockList.size() == 0 ? null : stockList;
	}
	
	/**
	 * Parse Stock History data
	 * @param symbolName
	 * @param line
	 * @return
	 * @throws NumberFormatException
	 * @throws ParseException
	 */
	private StockDetail parseStockDetail(String symbolName, String line) throws NumberFormatException, ParseException {
		String[] data = line.split(QUOTES_CSV_DELIMITER);
		String strDate = data[0];
		Date date;
		if (strDate.contains("-")) {
			date = new SimpleDateFormat("yyyy-MM-dd").parse(strDate);
		} else {
			date = new SimpleDateFormat(this.dateFormat).parse(strDate);
		}

		return new StockDetail(symbolName, date, Double.parseDouble(data[1]), Double.parseDouble(data[2]),
				Double.parseDouble(data[3]), Double.valueOf(decimalFormat.format(Double.parseDouble(data[4]))),
				Double.parseDouble(data[5]), Double.parseDouble(data[6]));
	}
	
	/**
	 * Add Business or working days to Given date
	 * @param baseDate
	 * @param numberOfDays
	 * @return
	 */
	public static Date addBusinessDays(Date baseDate, int numberOfDays) {

		if (baseDate == null) {
			baseDate = new Date();
		}

		Calendar baseDateCal = Calendar.getInstance();
		baseDateCal.setTime(baseDate);

		for (int i = 0; i < numberOfDays; i++) {

			baseDateCal.add(Calendar.DATE, 1);
			if (baseDateCal.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY) {
				baseDateCal.add(Calendar.DATE, 2);
			}
		}
		return baseDateCal.getTime();
	}

}
